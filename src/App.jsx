import { useState } from 'react'
import logo from './assets/agenda_icon.png'
import './App.css'
import { TasksList } from './components/taskslist';
import { nanoid } from 'nanoid';


function App() {

  // USESTATE
  // gère la liste des listes
  const [Lists, setLists] = useState(() => {
    const storedData = JSON.parse(localStorage.getItem('Lists'));
    return storedData || [];
    }
  );  
  // gère la liste courante
  const [currentList, setCurrentList] = useState(null);
  // gère les données entrées dans le formulaire d'ajout de tâche
  const [dataInput, setDataInput] = useState("");
  // gère les données entrées dans le formulaire d'ajout de liste
  const [newListName, setNewListName] = useState("");
  // gère la date
  const [currentDate, setCurrentDate] = useState(new Date());
  // gère le retour d'erreur en cas d'ajout de liste avec un nom vide
  const [errorNewListName, setErrorNewListName] = useState("");
  //gère le retour d'erreur en cas d'ajout de tâche avec un nom vide
  const [errorNewTaskName, setErrorNewTaskName] = useState("");
  // gère l'état coché ou non coché d'une tâche
  const [isChecked, setIsChecked] = useState('false');

  // FONCTIONS gérant les listes
  function createList(e){
    if (newListName.trim() !== '') {
      const newList = {
        id: `list-${nanoid()}`,
        listName: newListName,
        tasksList: []
      }
      const updatedList = [...Lists, newList];
      setLists(updatedList);
      localStorage.setItem('Lists', JSON.stringify(updatedList));
      setNewListName("");
      setCurrentList(newList);
      setErrorNewListName("");
      e.preventDefault();
    } else {
      setErrorNewListName("Veuillez renseigner un nom pour la nouvelle liste");
      e.preventDefault();
    }
  }

  function selectList(list) {
    setCurrentList(list);
  }

  function resetList() {
      const updatedListofLists = Lists.filter((l) => l.id !== currentList.id);
      localStorage.setItem('Lists', JSON.stringify(updatedListofLists));
      setLists(updatedListofLists);
      setCurrentList([]); 
  }

  // FONCTIONS gérant les tâches
  function SubmitTask(e) {
    if (dataInput.trim() !== '') {
      const newTask = {
        id: `task-${nanoid()}`,
        taskName: dataInput,
        checked: false
      }
      const updatedListofLists = Lists.map(list => {
        console.log('list.id :', list, ' - currentList.id : ', currentList.id);
        if (list.id === currentList.id) {
          const updatedList = { ...list, tasksList: [...list.tasksList, newTask] };
          setCurrentList(updatedList);
          return updatedList;
        } else {
          return list;
        }
      })
      setLists(updatedListofLists);
      localStorage.setItem('Lists', JSON.stringify(updatedListofLists));
      setDataInput("");
      setErrorNewTaskName("");
      e.preventDefault();
    } else {
      setErrorNewTaskName("Veuillez renseigner un nom pour la nouvelle tâche");
      e.preventDefault();
    }
  } 

  function RemoveTask(id) {
    const updatedListofLists = Lists.map(list => {
      if (list.id === currentList.id) {
        const newtasksList= currentList.tasksList.filter((t) => t.id !== id);
        const updatedList = { ...list, tasksList: newtasksList };
        setCurrentList(updatedList);
        return updatedList;
      } else {
        return list;
      }
    })
    
    setLists(updatedListofLists);
    localStorage.setItem('Lists', JSON.stringify(updatedListofLists));
  }

  function handleCheckboxChange(id) {
      if (id) {
        setIsChecked(!isChecked);
        const updatedListofLists = Lists.map(list => {
            if (list.id === currentList.id) {
              const newstasksList = list.tasksList.map((t) => {
                if (t.id === id) {
                  t.checked = !t.checked;
                }
                return t;
                })
              const updatedList = { ...list, tasksList: newstasksList };
              setCurrentList(updatedList);
              return updatedList;
            } else {
              return list;
            }
      })
      setLists(updatedListofLists);
      localStorage.setItem('Lists', JSON.stringify(updatedListofLists));
      }
    };   

  return (
    <main>
      {/* Entete de l'appli */}
      <div className='title'>
        <h1>To Do</h1>
        <div className='right-title'>
          <div className='date'>
            <h3>Today</h3>
            <p>{currentDate.toLocaleDateString()}</p>
          </div>
          <img src={logo} className="logo" alt="logo" />
        </div>
      </div>

      {/* Gestion des listes */}
        {/* Création d'une nouvele liste */}
      <div className='create-list'>
        <h3>Ajouter une liste</h3>
        <input type="text" onChange={(e) => { setNewListName(e.target.value) }} value={newListName}/>
        <button type="submit" onClick={createList}>+</button>
      </div>
      <div className='error'>{errorNewListName && <p>{errorNewListName}</p>}</div>
        {/* Affichage des listes existantes */}
      <div className='display-lists'>
        <ul>
          {Lists.map((list, index) => (
            <li key={index}>
              <button onClick={() => selectList(list)} className={currentList === list ? 'selected' : 'unselected'}>{list.listName}</button>
            </li>
          ))}
        </ul>
      </div>
      {/* Gestion de la liste courante */}
      <form className='add'>
        <input type="text" onChange={(e) => { setDataInput(e.target.value) }} value={dataInput}/>
        <button type="submit" onClick={SubmitTask}>+</button>
        <button type="reset" onClick={resetList}>Reset</button>
      </form>
      <div className='error'>{errorNewTaskName && <p>{errorNewTaskName}</p>}</div>
      <div className='list'>
        { currentList && <TasksList 
          currentList={currentList}
          onDelete={ RemoveTask }
          handleCheckbox={handleCheckboxChange}
        />}
        { !currentList && <p>Please select a list</p> }
      </div>
    </main>
  )
}

export default App