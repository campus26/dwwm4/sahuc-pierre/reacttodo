import { useState } from 'react'
import { Task } from './task'
import { saveToLocalStorage } from './manageLocalStorage';

export function TasksList(props) {
    if ((props.currentList.tasksList && props.currentList.tasksList.length > 0)) {
        const todo = props.currentList.tasksList.map((task) => (
                <Task 
                    taskName={task.taskName} 
                    checked={task.checked}
                    key={task.id}
                    id={task.id}
                    onDelete={ props.onDelete }
                    handleCheckbox={() => props.handleCheckbox(task.id)}
                />
        ));
        return todo;
    }  else {
        return <p>Aucune tâche dans la liste actuelle.</p>;
    }
}



