import { useState } from 'react'

export function Task(props) {

    return (
        <div>
          <input
            type="checkbox"
            id={props.id}
            name={props.taskName}
            checked={props.checked}
            onChange={ props.handleCheckbox }
          />
          <label htmlFor={props.id} className={props.checked ? 'checked' : ''}>
            {props.taskName}
          </label>
          <button className="removeButton" onClick={() => props.onDelete(props.id)}>X</button>
        </div>
      );

}