import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'

// const DATA = [
//   { id:"task-0", taskName: "Eat", checked: true },
//   { id:"task-1", taskName: "Sleep", checked: false },
//   { id:"task-2", taskName: "Repeat", checked: false },
// ];

ReactDOM.createRoot(document.getElementById('root')).render(
  // React.StrictMode mode ajoute de l'affichage en console, pas utile
  <React.StrictMode>
    {/* <App tasks={DATA}/> */}
    <App />
  </React.StrictMode>,
)
